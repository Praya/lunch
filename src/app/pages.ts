import { OfferEditPageComponent } from '@pages/offer-edit-page/offer-edit-page.component';
import { LoginPageComponent } from '@pages/login-page/login-page.component';
import { OfferPageComponent } from '@pages/offer-page/offer-page.component';
import { OffersPageComponent } from '@pages/offers-page/offers-page.component';
import { DistributorPageComponent } from '@pages/distributor-page/distributor-page.component';
import { Routes } from '@angular/router';
import { ProfilePageComponent } from '@pages/profile-page/profile-page.component';


export const appRoutes: Routes = [
    {
        path: '',
        component: LoginPageComponent,
    },
    {
        path: 'profile',
        component: ProfilePageComponent,
    },
    {
        path: 'distributor/:distributor',
        component: DistributorPageComponent,
    },
    {
        path: 'distributor/form/:distributor',
        component: DistributorPageComponent,
    },
    {
        path: 'offers/form/:offer',
        component: OfferEditPageComponent,
    },
    {
        path: 'offers',
        component: OffersPageComponent,
    },
];


export const pages = [
    LoginPageComponent,
    ProfilePageComponent,
    DistributorPageComponent,
    OfferEditPageComponent,
    OffersPageComponent,
    OfferPageComponent,
];
