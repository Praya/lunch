import { OfferFormComponent } from '@components/forms/offer-form/offer-form.component';
import { ProfileFormComponent } from '@components/forms/profile-form/profile-form.component';
import { ProfilePhotoSelectorComponent } from '@components/profile-photo-selector/profile-photo-selector.component';
import { ProfilePhotoComponent } from '@components/profile-photo/profile-photo.component';


export const components = [
    OfferFormComponent,
    ProfilePhotoComponent,
    ProfilePhotoSelectorComponent,
    ProfileFormComponent,
];
