import { ChangeDetectionStrategy, Component, forwardRef, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UcWidgetComponent } from 'ngx-uploadcare-widget';


@Component({
    selector: 'app-image-selector',
    templateUrl: './image-selector.component.html',
    styleUrls: ['./image-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => ImageSelectorComponent),
        multi: true
    }],
})
export class ImageSelectorComponent implements ControlValueAccessor {

    @Input()
    public value: string;

    @ViewChild(UcWidgetComponent)
    private ucWidgetComponent: UcWidgetComponent;

    private controlOnChangeCallback: any;
    private controlOnTouchedCallback: any;

    public setValue(value: string): void {
        this.value = value;

        if (this.controlOnChangeCallback instanceof Function) {
            this.controlOnChangeCallback(value);
        }

        if (this.controlOnTouchedCallback instanceof Function) {
            this.controlOnTouchedCallback();
        }

    }

    public writeValue(value: string): void {
        this.value = value;
    }

    public registerOnChange(onChangeCallback: any): void {
        this.controlOnChangeCallback = onChangeCallback;
    }

    public registerOnTouched(onTouchedCallback: any): void {
        this.controlOnTouchedCallback = onTouchedCallback;
    }

}
