import { ChangeDetectionStrategy, Component, Input } from '@angular/core';


@Component({
    selector: 'app-image',
    templateUrl: './image.component.html',
    styleUrls: ['./image.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageComponent {

    @Input()
    public image: string;

    public defaultPreview = '/assets/profile/default-profile-photo.png';
    public modifierLarge = '-/scale_crop/1280x720/';
    public modifierSmall = '-/scale_crop/220x220/';

    public getLargeUrl(): string {
        return this.image ? this.image + this.modifierLarge : this.defaultPreview;
    }

    public getPreviewUrl(): string {
        return this.image ? this.image + this.modifierSmall : this.defaultPreview;
    }

}
