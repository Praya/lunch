import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Distributor } from '@models/Distributor';
import { AngularFirestore } from 'angularfire2/firestore';


@Component({
    selector: 'app-distributor-form',
    templateUrl: './distributor-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DistributorFormComponent implements OnInit {

    @Input()
    public distributorData: Distributor;

    @Output()
    public remove = new EventEmitter<void>();

    @Output()
    public save = new EventEmitter<Distributor>();

    @Output()
    public cancel = new EventEmitter<void>();

    public form: FormGroup;

    constructor(private afs: AngularFirestore, private fb: FormBuilder, private router: Router) {}

    public ngOnInit(): void {
        this.form = this.fb.group({
            address: [this.distributorData.address, [
                Validators.required,
                Validators.maxLength(256),
            ]],
            description: [this.distributorData.description, [
                Validators.required,
                Validators.maxLength(1024),
            ]],
            name: [this.distributorData.name, [
                Validators.required,
                Validators.maxLength(32),
            ]],
            legalName: [this.distributorData.legalName, [
                Validators.required,
                Validators.maxLength(32),
            ]],
            phone: [this.distributorData.phone, [
                Validators.required,
            ]],
            photo: [this.distributorData.photo, [
                Validators.required,
            ]],
            site: [this.distributorData.site, []],
        });
    }

}
