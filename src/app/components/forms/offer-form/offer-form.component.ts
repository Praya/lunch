import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Offer } from 'app/models/Offer';

@Component({
    selector: 'app-offer-form',
    templateUrl: './offer-form.component.html',
    styleUrls: ['./offer-form.component.scss'],
})
export class OfferFormComponent implements OnInit, OnDestroy {

    @Input('offer')
    public offer: Offer;

    @Output()
    public change: EventEmitter<Offer> = new EventEmitter<Offer>();

    public offerForm: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    public ngOnInit(): void {
        this.offerForm = this.fb.group({
            title: [this.offer.title, [Validators.maxLength(42), Validators.required]],
            description: [this.offer.description, [Validators.maxLength(256)]],
            price: [this.offer.price, [Validators.required, Validators.pattern(/^[0-9]+$/)]],
            weight: [this.offer.weight, [Validators.pattern(/^[0-9]*$/)]],
            volume: [this.offer.volume, [Validators.pattern(/^[0-9]*$/)]],
        });

    }

    public ngOnDestroy(): void {
    }

    onSubmit() {
        console.log(this.offerForm);
    }

    revert() {
    }

}
