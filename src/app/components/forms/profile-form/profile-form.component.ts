import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Profile } from '@models/Profile';

@Component({
    selector: 'app-profile-form',
    templateUrl: './profile-form.component.html',
    styleUrls: ['./profile-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileFormComponent implements OnInit {

    @Input()
    public profileData: Profile;

    public profileForm: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    public ngOnInit(): void {
        this.profileForm = this.fb.group({
            firstName: [this.profileData.firstName, [Validators.maxLength(32), Validators.required]],
            lastName: [this.profileData.lastName, [Validators.maxLength(32), Validators.required]],
            photo: [this.profileData.photo],
            phone: {value: this.profileData.phone, disabled: true},
        });
    }

    public isNotChanged(): boolean {
        const profileFormData: Profile = this.profileForm.getRawValue();

        return profileFormData.firstName === this.profileData.firstName &&
            profileFormData.lastName === this.profileData.lastName &&
            profileFormData.phone === this.profileData.phone &&
            profileFormData.photo === this.profileData.photo;
    }

}
