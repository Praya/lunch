import { Component } from '@angular/core';
import * as firebase from 'firebase';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;

@Component({
    selector: 'app-distributor-full',
    templateUrl: './distributor-full.component.html',
    styleUrls: ['./distributor-full.component.css'],
})
export class DistributorFullComponent {

    public distributor: QueryDocumentSnapshot;

}
