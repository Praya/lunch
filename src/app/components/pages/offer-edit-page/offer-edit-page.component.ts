import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfferFormComponent } from '@components/forms/offer-form.component';
import { Offer } from '@models/Offer';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore/collection/collection';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;


@Component({
    selector: 'app-offer-edit-page',
    templateUrl: './offer-edit-page.component.html',
    styleUrls: ['./offer-edit-page.component.scss'],
})
export class OfferEditPageComponent implements OnInit, OnDestroy {

    @ViewChild(OfferFormComponent)
    public offerForm: OfferFormComponent;
    public offer$: Observable<DocumentSnapshot>;
    private offersCollection: AngularFirestoreCollection<Offer> = this.afs.collection<Offer>('offers');

    constructor(private route: ActivatedRoute, private afs: AngularFirestore) {

        this.offer$ = this.route.params.switchMap(params => {
            return this.offersCollection.doc<Offer>(params.offer).changes();
        });

    }

    public async ngOnInit(): Promise<void> {
        // this.getOffer().subscribe(console.warn)
    }

    public ngOnDestroy(): void {
    }

}
