import { Component } from '@angular/core';
import { Offer } from '@models/Offer';
import { AngularFirestore } from 'angularfire2/firestore';
import { DocumentChangeAction } from 'angularfire2/firestore/interfaces';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-offers-page',
    templateUrl: './offers-page.component.html',
    styleUrls: ['./offers-page.component.scss'],
})
export class OffersPageComponent {

    public offers$: Observable<Array<Offer>>;

    constructor(public afs: AngularFirestore) {

        this.offers$ = this.getCollection<Offer>('offers');

    }

    getCollection<T>(collectionName: string): Observable<Array<T>> {
        return this.afs.collection<T>(collectionName).snapshotChanges()
            .map<DocumentChangeAction[], Array<T>>(actions => {
                return actions.map<T>(({payload: {doc}}) => (<any>{
                    _id: doc.id,
                    ...doc.data(),
                }));
            });
    }

}
