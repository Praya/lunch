import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthService } from 'app/services/auth.service';

@Component({
    selector: 'app-user-page',
    templateUrl: './user-page.component.html',
    styleUrls: ['./user-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserPageComponent {


    constructor(public authService: AuthService) {
        // todo: add form builder
    }

    public saveUser(): void {
        // todo: save user
    }

    public signOut(): void {
        this.authService.signOut();
    }

}
