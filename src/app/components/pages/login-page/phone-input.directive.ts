import { Directive, ElementRef, EventEmitter, HostListener, Output, } from '@angular/core';

@Directive({
  selector: '[appPhoneInput]',
})
export class PhoneInputDirective {

  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();

  constructor(private elementRef: ElementRef) {
  }

  @HostListener('input')
  public onValueChange(): void {
    this.elementRef.nativeElement.value = '+' + this.elementRef.nativeElement.value.replace(/[^0-9]+/g, '');
    this.valueChange.next(this.elementRef.nativeElement.value);
  }

}
