import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { PhoneInputDirective } from './phone-input.directive';
import { AuthService } from 'app/services/auth.service';
import { User } from 'firebase/app';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PhoneInputDirective],
})
export class LoginPageComponent implements OnInit, OnDestroy {

  public user$: Observable<User | undefined>;
  public showLoginForm$: Observable<boolean>;
  public authFormDisplay$: Observable<string>;
  public confirmationFormDisplay$: Observable<string>;
  public signOutButtonDisplay$: Observable<string>;

  private subscriptionForCdr: Subscription;

  constructor(public authService: AuthService, private cdr: ChangeDetectorRef) {

    this.showLoginForm$ = this.authService.user$.map((user: any) => {
      return Boolean(user);
    });
    this.user$ = this.authService.user$;

    this.authFormDisplay$ = Observable.combineLatest(this.authService.user$, <any>this.authService.confirmationIsRequired$)
      .map(([user, confirmationRequired]) => {
        console.log(user, confirmationRequired);
        return !user && !confirmationRequired ? 'block' : 'none';
      });

    this.confirmationFormDisplay$ = Observable.combineLatest(
      this.authService.user$.map(user => Boolean(user)),
      this.authService.confirmationIsRequired$,
    ).map(([user, confirmationRequired]) => {
      return !user && confirmationRequired ? 'block' : 'none';
    });

    this.signOutButtonDisplay$ = this.authService.user$.map((user) => user ? 'block' : 'none');

  }

  public ngOnInit(): void {

    this.authService.bindAntiRobotToButton('sign-in-button');

    this.subscriptionForCdr = Observable.combineLatest(
      this.authService.error$,
      this.authService.user$,
      this.authService.confirmationIsRequired$,
    ).delay(1).subscribe(() => {
      this.cdr.detectChanges();
    });

  }

  public ngOnDestroy(): void {
    this.authService.unbindAntiRobot();
    this.subscriptionForCdr.unsubscribe();
  }

  public signIn(phone: string): void {
    this.authService.signIn(phone);
  }

  public confirmSigningIn(code: string): void {
    this.authService.confirmSigningIn(code);
  }

}
