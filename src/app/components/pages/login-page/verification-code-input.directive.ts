import { Directive, ElementRef, EventEmitter, HostListener, Output, } from '@angular/core';

@Directive({
  selector: '[appVerificationCodeInput]',
})
export class VerificationCodeInputDirective {

  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>();
  @Output() codeEntered: EventEmitter<string> = new EventEmitter<string>();

  constructor(private elementRef: ElementRef) {
  }

  @HostListener('input')
  public onValueChange(): void {

    const value: string = this.elementRef.nativeElement.value
      .replace(/[^0-9]+/g, '')
      .slice(0, 6);

    this.elementRef.nativeElement.value = value;
    this.valueChange.next(value);

    if (value.length === 6) {
      this.codeEntered.next(value);
    }
  }

}
