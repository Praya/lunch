import { Component } from '@angular/core';
import { AuthService } from '@services/auth.service';
import { CurrentUserService } from '@services/current-user.service';
import { DistributorsService } from '@services/distributors/distributors.service';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {

    public userPhoneNumber$: Observable<string>;
    public distributors$: Observable<Array<QueryDocumentSnapshot> | undefined>;
    public firstDistributor$: Observable<QueryDocumentSnapshot | undefined>;

    constructor(private authService: AuthService,
                private distributorService: DistributorsService,
                private currentUserService: CurrentUserService) {

        this.userPhoneNumber$ = this.authService.user$.map(user => user && user.phoneNumber ? user.phoneNumber : '');

        this.distributors$ = this.currentUserService.distributors$;

        this.firstDistributor$ = this.currentUserService.distributors$.map(distributors => distributors ? distributors[0] : undefined);

    }

}
