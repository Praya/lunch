export interface Profile {
    firstName: string;
    lastName: string;
    photo: string;
    phone: string;
}
