export interface Offer {
    title: string;
    description: string;
    price: number;
    weight: number | undefined;
    volume: number | undefined;
}
