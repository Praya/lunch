export interface Distributor {
    name: string;
    description: string;
    ownerId: string;
    placeId: string;
    logo: string;
}
