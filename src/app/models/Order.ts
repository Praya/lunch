import { Offer } from './Offer';

export interface Order {
    offer: Offer;
    time: number;
}
