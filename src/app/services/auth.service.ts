import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import * as firebase from 'firebase/app';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AngularFirestore } from 'angularfire2/firestore';
import { DocumentReference } from '@firebase/firestore-types';
import RecaptchaVerifier = firebase.auth.RecaptchaVerifier;

@Injectable()
export class AuthService {

    public user$: BehaviorSubject<User | undefined>;
    public userRef$: Observable<DocumentReference | undefined>;
    public error$: Subject<string> = new Subject<string>();
    public confirmationIsRequired$: Observable<boolean>;

    private applicationVerifier: any;
    private confirmation$: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

    constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth) {
        this.afAuth = afAuth;

        this.confirmationIsRequired$ = this.confirmation$.map((confirmation) => Boolean(confirmation));

        this.user$ = new BehaviorSubject<any>(this.afAuth.auth.currentUser);
        this.afAuth.auth.onAuthStateChanged(() => {
            this.user$.next(this.afAuth.auth.currentUser ? this.afAuth.auth.currentUser : undefined);
        });

        this.userRef$ = this.user$.map((authUser: User) => {
            return authUser ? this.afs.collection('users').doc(authUser.uid).ref : undefined;
        });

    }

    public bindAntiRobotToButton(buttonId: string): void {
        this.applicationVerifier = new RecaptchaVerifier(buttonId, { size: 'invisible' });
    }

    public unbindAntiRobot(): void {

        if (this.applicationVerifier) {
            this.applicationVerifier.clear();
            this.applicationVerifier = undefined;
        }

        if (this.confirmation$.getValue()) {
            this.confirmation$.next(undefined);
        }

    }

    public signOut(): Promise<any> {
        return this.afAuth.auth.signOut();
    }

    public confirmSigningIn(code: string): void {

        if (this.confirmation$.getValue()) {
            this.confirmation$.getValue().confirm(code)
                .then((result: any) => {
                    console.log(result);
                })
                .catch(() => {
                    this.error$.next('confirmation failed');
                });
        }

    }

    public signIn(phoneNumber: string): void {
        this.afAuth.auth
            .signInWithPhoneNumber(phoneNumber, this.applicationVerifier)
            .then((confirmation) => {
                this.confirmation$.next(confirmation);
            })
            .catch((error) => {
                console.log(error.message);
                this.error$.next(error.message);
            });
    }

}
