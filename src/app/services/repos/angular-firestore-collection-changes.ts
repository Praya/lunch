import { AngularFirestoreCollection } from 'angularfire2/firestore/collection/collection';
import { AngularFirestoreDocument } from 'angularfire2/firestore/document/document';
import { DocumentChangeAction } from 'angularfire2/firestore/interfaces';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;


export interface FirestoreDocument<T> extends QueryDocumentSnapshot {

    data(): T;

}

declare module 'angularfire2/firestore/collection/collection' {

    interface AngularFirestoreCollection<T> {
        changes(): Observable<Array<QueryDocumentSnapshot>>;
    }

}


AngularFirestoreCollection.prototype.changes = function () {
    return this.snapshotChanges()
        .map(actions => {
            return actions.map((documentChangeAction: DocumentChangeAction) => {
                return documentChangeAction.payload.doc;
            });
        });
};


declare module 'angularfire2/firestore/document/document' {

    interface AngularFirestoreDocument<T> {
        changes(): Observable<DocumentSnapshot>;
    }

}


AngularFirestoreDocument.prototype.changes = function () {
    return this.snapshotChanges().map(action => action.payload);
};
