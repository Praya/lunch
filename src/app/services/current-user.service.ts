import { Injectable } from '@angular/core';
import { Profile } from '@models/Profile';
import { AuthService } from '@services/auth.service';
import { DistributorsService } from '@services/distributors/distributors.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { User } from 'firebase';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;


@Injectable()
export class CurrentUserService {

    public user$: Observable<User | undefined>;
    public profile$: Observable<DocumentSnapshot | undefined>;
    public distributors$: Observable<Array<QueryDocumentSnapshot> | undefined>;

    constructor(private afs: AngularFirestore,
                private authService: AuthService,
                private distributorsService: DistributorsService) {

        this.user$ = this.authService.user$;

        this.profile$ = this.authService.user$.switchMap((user: User | undefined) => {
            return user ? this.afs.collection<Profile>('profiles').doc(user.uid).changes() : Observable.of(undefined);
        });

        this.distributors$ = this.profile$.switchMap((profile: QueryDocumentSnapshot | undefined) => {
            return profile ? this.distributorsService.getDistributorsByOwner(profile.ref) : Observable.of(undefined);
        });

    }

}
