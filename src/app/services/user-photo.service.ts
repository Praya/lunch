import { Injectable } from '@angular/core';
import { AuthService } from '@services/auth.service';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { User } from 'firebase/app';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserPhotoService {

    public photoRef$: BehaviorSubject<AngularFireStorageReference | undefined> = new BehaviorSubject(undefined);
    public uploadTask$: BehaviorSubject<AngularFireUploadTask | undefined>
        = new BehaviorSubject<AngularFireUploadTask | undefined>(undefined);
    public uploadPhotoProgress$: Observable<number | undefined>;
    public photoIsUploading$: Observable<boolean>;
    public photo$: Observable<string>;
    public photoOrPlaceHolder$: Observable<string>;
    public photoOrPlaceHolderOrSpinner$: Observable<string>;

    constructor(private authService: AuthService,
                private afStorage: AngularFireStorage) {

        this.authService.user$
            .map(user => user ? this.afStorage.ref(`userPhoto/small/${user.uid}`) : undefined)
            .subscribe(ref => this.photoRef$.next(ref));

        this.photo$ = this.photoRef$
            .switchMap(photoRef => photoRef ? photoRef.getDownloadURL() : Observable.of(''))
            .catch(() => Observable.of(''))
            .map(url => url ? `${url}&cache-breaker=${Math.random()}` : '');

        this.photoOrPlaceHolder$ = this.photo$
            .map(photo => photo ? photo : '/assets/user/default-user-photo.png');

        this.uploadPhotoProgress$ = this.uploadTask$
            .switchMap(task => task ? task.percentageChanges() : Observable.of(undefined));

        this.photoIsUploading$ = this.uploadPhotoProgress$
            .map(progress => typeof progress === 'number' && progress < 100)
            .distinctUntilChanged();

        this.photoOrPlaceHolderOrSpinner$ = this.photoIsUploading$
            .switchMap(photoIsUploading => photoIsUploading ? Observable.timer(2000).mapTo(true) : Observable.of(photoIsUploading))
            .switchMap(uploading => uploading ? Observable.of('/assets/spinner.gif') : this.photoOrPlaceHolder$);

    }

    uploadUserPhoto(photo: any): AngularFireUploadTask {

        const user: User | undefined = this.authService.user$.getValue();

        if (!user) {
            throw new Error('User is not available');
        }

        const task: AngularFireUploadTask = this.afStorage.upload(`/userPhoto/origin/${user.uid}`, photo);
        this.uploadTask$.next(task);

        return task;
    }

    removeUserPhoto() {
        const photoRef: AngularFireStorageReference | undefined = this.photoRef$.getValue();

        if (!photoRef) {
            throw new Error('Photo is not available');
        }

        photoRef.delete().subscribe(() => {
            this.photoRef$.next(this.photoRef$.getValue());
        });
    }

}
