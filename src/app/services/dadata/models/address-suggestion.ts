import AddressSuggestionData from '@services/dadata/models/address-suggestion-data';

export default interface AddressSuggestion {
    value: string;
    unrestricted_value: string;
    data: AddressSuggestionData;
}
