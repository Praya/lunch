import { Gender } from '@services/dadata/models/gender';

export default interface FioSuggestionData {
    surname: string | null;
    name: string | null;
    patronymic: string | null;
    gender: Gender | null;
    qc:  null;
}
