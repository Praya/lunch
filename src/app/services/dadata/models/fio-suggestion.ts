import FioSuggestionData from '@services/dadata/models/fio-suggestion-data';

export default interface FioSuggestion {
    value: string;
    unrestricted_value: string;
    data: FioSuggestionData;
}
