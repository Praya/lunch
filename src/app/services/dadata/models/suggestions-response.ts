export default interface SuggestionsResponse<T> {
    suggestions: Array<T>;
}
