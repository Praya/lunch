import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import AddressSuggestion from '@services/dadata/models/address-suggestion';
import FioSuggestion from '@services/dadata/models/fio-suggestion';
import SuggestionsResponse from '@services/dadata/models/suggestions-response';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DadataSuggestionsService {

    private apiKey = '8f87651f308cf37f0fec5b526084d491d98d9762';

    private urls = {
        fio: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/fio',
        address: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
        party: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party',
        bank: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/bank',
        email: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/email',
    };

    constructor(private http: HttpClient) {

    }

    public getFioSuggestions(query: string, count: number): Observable<SuggestionsResponse<FioSuggestion>> {
        return this.getSuggestions<FioSuggestion>('fio', query, count);
    }

    public getAddressSuggestions(query: string, count: number): Observable<SuggestionsResponse<AddressSuggestion>> {
        return this.getSuggestions<AddressSuggestion>('address', query, count);
    }

    public getSuggestions<T>(type: string, query: string, count: number): Observable<SuggestionsResponse<T>> {

        if (!this.urls[type]) {
            throw new Error(`Unknown type "${type}"`);
        }

        return this.sendRequest(this.urls[type], {query, count});
    }

    private sendRequest<T>(url, body: Object): Observable<T> {

        const headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Token ${this.apiKey}`,
        });

        return <Observable<T>>this.http.post(url, JSON.stringify(body), {headers});
    }

}
