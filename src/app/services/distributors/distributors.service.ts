import { Injectable } from '@angular/core';
import { Distributor } from '@models/Distributor';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { DocumentReference } from '@firebase/firestore-types';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;

@Injectable()
export class DistributorsService {

    constructor(private afs: AngularFirestore) {
    }

    public getDistributorsByOwner(ownerRef: DocumentReference): Observable<Array<QueryDocumentSnapshot>> {
        return this.afs.collection<Distributor>('distributors', ref => {
            return ref.where('ownerRef', '==', ownerRef);
        }).changes();
    }

}
