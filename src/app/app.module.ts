import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/distinctUntilChanged';
import { PhoneInputDirective } from '@pages/login-page/phone-input.directive';
import { VerificationCodeInputDirective } from '@pages/login-page/verification-code-input.directive';
import { AuthService } from '@services/auth.service';
import { CurrentUserService } from '@services/current-user.service';
import { DistributorsService } from '@services/distributors/distributors.service';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
} from '@angular/material';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { appRoutes, pages } from './pages';
import { components } from './components';
import '@repos/angular-firestore-collection-changes';
import { UcWidgetComponent } from 'ngx-uploadcare-widget';


@NgModule({
    declarations: [
        AppComponent,
        PhoneInputDirective,
        VerificationCodeInputDirective,
        UcWidgetComponent,
        ...components,
        ...pages,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forRoot(appRoutes, {enableTracing: true} /* <-- debugging purposes only */),
        FlexLayoutModule,
        BrowserAnimationsModule,

        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        MatDividerModule,
        MatListModule,

        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AngularFireStorageModule,
    ],
    providers: [
        AuthService,
        DistributorsService,
        CurrentUserService,
        PhoneInputDirective,
        VerificationCodeInputDirective,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
