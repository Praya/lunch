import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { CurrentUserService } from 'app/services/current-user.service';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;

@Component({
    selector: 'app-distributor-page',
    templateUrl: './distributor-page.component.html',
    styleUrls: ['./distributor-page.component.css'],
})
export class DistributorPageComponent {

    public distributor$: Observable<QueryDocumentSnapshot | undefined>;

    constructor(private route: ActivatedRoute,
                private authService: AuthService,
                private currentUserService: CurrentUserService) {

        const distributorId: string | null = route.snapshot.paramMap.get('distributor');

        if (!distributorId) {
            throw new Error('distributor id is not defined in route');
        }

        this.distributor$ = this.currentUserService.distributors$
            .map(distributors => distributors ? distributors[0] : undefined);

    }

}
