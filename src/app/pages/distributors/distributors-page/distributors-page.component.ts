import { Component } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { CurrentUserService } from 'app/services/current-user.service';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;

@Component({
    selector: 'app-distributors-page',
    templateUrl: './distributors-page.component.html',
    styleUrls: ['./distributors-page.component.css'],
})
export class DistributorsPageComponent {

    public distributors$: Observable<Array<QueryDocumentSnapshot> | undefined>;

    constructor(private authService: AuthService,
                private currentUserService: CurrentUserService) {

        this.distributors$ = this.currentUserService.distributors$;

    }

}
