import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { CurrentUserService } from '@services/current-user.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from 'app/services/auth.service';
import * as firebase from 'firebase';
import { UcWidgetComponent } from 'ngx-uploadcare-widget';
import { Observable } from 'rxjs/Observable';
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;

@Component({
    selector: 'app-user-page',
    templateUrl: './profile-page.component.html',
    styleUrls: ['./profile-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilePageComponent {

    public profile$: Observable<DocumentSnapshot | undefined>;

    @ViewChild(UcWidgetComponent)
    private ucWidgetComponent: UcWidgetComponent;


    constructor(public afs: AngularFirestore,
                public currentUserService: CurrentUserService,
                private authService: AuthService) {

        this.profile$ = this.currentUserService.profile$;

    }

    public saveUser(): void {
        // this.afs.collection('profiles').doc()
    }

    public signOut(): void {
        this.authService.signOut();
    }

}
