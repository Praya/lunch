import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));


window['UPLOADCARE_PUBLIC_KEY'] = '745b544e0fc77bb419c6';
window['UPLOADCARE_LOCALE'] = 'ru';
